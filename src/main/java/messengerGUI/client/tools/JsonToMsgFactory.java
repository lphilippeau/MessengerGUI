package messengerGUI.client.tools;

import messengerGUI.client.model.Message;
import messengerGUI.client.model.ReadOnceMessage;
import messengerGUI.client.model.RegMessage;


import java.util.ArrayList;
import java.util.HashMap;

/**
 * Utility class to receive, deserialize, and create Message objects
 * Created by Luigi Philippeau on 12/4/2016.
 */
public class JsonToMsgFactory {

    public static HashMap<String, ArrayList<Message>> buildMessageMap(String[] jsonMessages){

        HashMap<String, ArrayList<Message>> messageMap = new HashMap<>();

        //Loop through the json array and add the correct objects to the map
        for(String msg : jsonMessages){
            //if it is a readOnce message
            if(msg.contains("canRead")){
                ReadOnceMessage readOnce = ReadOnceMessage.deserialize(msg);
                //check if the map contains this key already
                if(!messageMap.keySet().contains(readOnce.getSender())) messageMap.put(readOnce.getSender(), new ArrayList<Message>());
                //add the message to the list
                messageMap.get(readOnce.getSender()).add(readOnce);
            } else {
                //it is a regular message
                RegMessage regMsg = RegMessage.deserialize(msg);
                //check if the map contains this key already
                if(!messageMap.keySet().contains(regMsg.getSender())) messageMap.put(regMsg.getSender(), new ArrayList<Message>());
                messageMap.get(regMsg.getSender()).add(regMsg);
            }
        }

        return messageMap;

    }
}
