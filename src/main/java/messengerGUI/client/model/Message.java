package messengerGUI.client.model;

import java.util.Date;

/**
 * Created by Luigi Philippeau on 12/2/2016.
 */
public abstract class Message implements Sendable {

    public abstract String getSender();

    public abstract String getReceiver();

    public abstract Date getDateSent();

    public abstract void setDateSent(Date date);

    public String toString(){
        String s = getSender() +
                ": " + getPayload();
        return s;
    }

}
