package messengerGUI.client.model;

/**
 * This is the interface that all different messages must implement
 * Created by Luigi Philippeau on 11/12/2016.
 */
public interface Sendable {

    /**
     * This is the message that will be read by the user
     * @return
     */
    public String getPayload();
}
