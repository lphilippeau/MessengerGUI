package messengerGUI.client.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is the representation of each profile created in the messenger
 * Created by Luigi Philippeau on 11/13/2016.
 */
@XmlRootElement
public class Profile {

    private String name;
    private String username;
    private String password;

    /*no op constructor for deserialization*/
    public Profile(){}

    /**
     * Creates a profile object
     * @param name - the name of the user
     * @param username - the screen name of the user
     * @param password - the password of the user
     */
    public Profile(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Takes a json string and returns a profile object
     * @param jsonProfile - the json representation of the profile
     * @return instance of the deserialized profile
     */
    public static Profile deserialize(String jsonProfile){

        Gson gson = new GsonBuilder().create();
        Profile profile = gson.fromJson(jsonProfile, Profile.class);
        return profile;
    }

    public String toString(){
        String s = "Name: " + name + "\n" +
                "Username: " + username + "";
        return s;
    }
}
