package messengerGUI.client.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * This class represents the messages that will be send back and forth in the application
 * Created by Luigi Philippeau on 11/29/2016.
 */
@XmlRootElement
public class RegMessage extends Message {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String payload;
    String sender;
    String receiver;
    Date dateSent;

    public RegMessage(){}

    /**
     * Constructs a new regular message
     * @param sender - the person who sent the message
     * @param receiver - the person the message is meant for
     * @param payload - the content of the message
     */
    public RegMessage(String sender, String receiver, String payload){
        this.sender = sender;
        this.receiver = receiver;
        this.dateSent = new Date();
        this.payload = payload;

    }

    @Override
    public String getPayload() {
        return payload;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    @Override
    public void setDateSent(Date date) {
        this.dateSent = date;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public static RegMessage deserialize(String jsonProfile){

        Gson gson = new GsonBuilder().create();
        RegMessage message = gson.fromJson(jsonProfile, RegMessage.class);
        return message;
    }
}
