package messengerGUI.client.model;

import messengerGUI.client.model.*;

import java.sql.SQLException;

public class SimpleMessageFactory {

    public SimpleMessageFactory(){

    }

    public static Message createMessage(String sender, String receiver, String payload, String readOnce) throws SQLException {
        if (readOnce.equals("readOnce")) {
            return new ReadOnceMessage(sender, receiver, payload);
        } else if (readOnce.equals("regular")) {
            return new RegMessage(sender, receiver, payload);
        }
        throw new SQLException("Cannot send this tyoe if message");
    }

}
