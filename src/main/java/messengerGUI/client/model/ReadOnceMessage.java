package messengerGUI.client.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

/**
 * This class represents a message who's payload can only be seen once
 * Created by Luigi Philippeau on 11/29/2016.
 */
public class ReadOnceMessage extends Message {

    /**
	 * 
	 */
	protected String payload;
    boolean canRead;
    String sender;
    String receiver;
    Date dateSent;

    /**
     * Constructs a new regular message
     * @param sender - the person who sent the message
     * @param receiver - the person the message is meant for
     * @param payload - the content of the message
     */
    public ReadOnceMessage(String sender, String receiver, String payload){

        this.sender = sender;
        this.receiver = receiver;
        this.dateSent = new Date();
        this.payload = payload;
        canRead = true; //true at first or else this would never be read

    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public boolean isCanRead() {
        return canRead;
    }

    public void setCanRead(boolean canRead){
        this.canRead = false;
    }

    @Override
    public String getPayload() {
        return payload;
    }

    @Override
    public void setDateSent(Date date) {
        this.dateSent = date;
    }

    /**
     * Takes a json string and returns a readOnceMessage object
     * @param jsonMessage - the json representation of the message
     * @return instance of the deserialized message
     */
    public static ReadOnceMessage deserialize(String jsonMessage){

        Gson gson = new GsonBuilder().create();
        ReadOnceMessage message = gson.fromJson(jsonMessage, ReadOnceMessage.class);
        return message;
    }
}
