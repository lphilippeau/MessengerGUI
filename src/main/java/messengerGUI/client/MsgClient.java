package messengerGUI.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import messengerGUI.client.model.Message;
import messengerGUI.client.model.Profile;
import messengerGUI.client.tools.JsonToMsgFactory;
import sun.misc.BASE64Encoder;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Rest client that communicates with messenger server
 * Created by Luigi Philippeau on 11/27/2016.
 */
@SuppressWarnings("restriction")
public class MsgClient{

    //// TODO: 11/30/2016 Add a dedicated method to set the auth token.  
    final String BASE_URI = "http://localhost:8082/Messenger/webapi/"; //Point this to the Messenger Server
    String HEADER_PREFIX = "Basic ";
    String basicAuthToken; //needed for secured apis
    public static Profile sessionProfile; //the logged in user

    public Client client;

    public MsgClient(){
        //instantiate the rest client and point it to the server
        this.client = ClientBuilder.newClient();
    }

    /**
     * Creates an account on the server
     *
     * @param name - the name of the account owner
     * @param username - the screen name for the account
     * @param password - the account password
     * @return - the registered user profile
     */
    public Profile register(String name, String username, String password) throws Exception {
        //put needed params into a map
        MultivaluedHashMap<String, String> formParams = new MultivaluedHashMap<>();
        formParams.add("username", username);
        formParams.add("name", name);
        formParams.add("password", password);

        //point the client to the profile api
        WebTarget profileAPI = client.target(BASE_URI).path("profile");
        Response response = profileAPI.request(MediaType.APPLICATION_XML).buildPost(Entity.form(formParams)).invoke();


        if(!(response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL)){
            throw new Exception(response.getStatusInfo().getReasonPhrase());
        }
        return response.readEntity(Profile.class);

    }

    /**
     * Logs the user into the application
     * @param username - the username
     * @param password - the password
     * @return - the logged in user profile
     */
    public Profile login(String username, String password) throws Exception {

        //Store the username and password
        MultivaluedHashMap<String, Object> headerParams = new MultivaluedHashMap<>();
        headerParams.add("username", username);
        headerParams.add("password", password);

        //point to the profile api
        WebTarget profileAPI = client.target(BASE_URI).path("profile");
        Response response = profileAPI.request(MediaType.TEXT_PLAIN).headers(headerParams).get();

        if(!(response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL)){
            throw new Exception(response.getStatusInfo().getReasonPhrase());
        }

        //Name of the account owner. Logged in successfully
        String ownerName = response.readEntity(String.class);

        //return new Profile(ownerName, username, password);

        //save the logged in user profiel
        sessionProfile = new Profile(ownerName, username, password);

        //save the authorization token
        String encode = new BASE64Encoder().encode((username.concat(":").concat(password)).getBytes());
		String encodedUserPass = encode;
        basicAuthToken = HEADER_PREFIX + encodedUserPass;

        return sessionProfile;
    }

    public boolean isUser(String username){
        //Store the username and password
        MultivaluedHashMap<String, Object> headerParams = new MultivaluedHashMap<>();
        headerParams.add("username", username);

        //point to the profile api
        WebTarget profileAPI = client.target(BASE_URI).path("profile").path("isUser");
        Response response = profileAPI.request(MediaType.TEXT_PLAIN).headers(headerParams).get();

        //Name of the account owner. Logged in successfully
        String isUser = response.readEntity(String.class);

        return isUser.equals("yes");
    }

    /**
     * Sends a new message to a user
     * @param sender
     * @param receiver
     * @param payload
     * @param type
     * @return
     */
    public String sendMessage(String sender, String receiver, String payload, String type) throws Exception{

        //todo add error checking here. finish javadoc
        //this.login(sender, "Philippesau");//authorization works
        //put all of the message fields into a map
        MultivaluedHashMap<String, String> formParams = new MultivaluedHashMap<>();
        formParams.add("sender", sender);
        formParams.add("receiver", receiver);
        formParams.add("payload", payload);
        formParams.add("type", type);

        //auth header params
        MultivaluedHashMap<String, Object> headerParams = new MultivaluedHashMap<>();
        headerParams.add("Authorization", basicAuthToken);

        //point the client to the profile api
        WebTarget messageAPI = client.target(BASE_URI).path("messages");
        Response response = messageAPI.request(MediaType.TEXT_PLAIN).headers(headerParams).buildPost(Entity.form(formParams)).invoke();

        if(!(response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL)){
            throw new Exception(response.getStatusInfo().getReasonPhrase());
        }

        return response.readEntity(String.class);

    }

    /**
     * Returns all messages in the user's inbox
     * @param requester the logged in user
     * @return all messages from all of the users being talked to
     * @throws Exception
     */
    public HashMap<String, ArrayList<Message>> getAllMessages(String requester) throws Exception{

        //auth header params
        MultivaluedHashMap<String, Object> headerParams = new MultivaluedHashMap<>();
        headerParams.add("Authorization", basicAuthToken);
        headerParams.add("requester", requester);

        //point the client to the profile api
        WebTarget messageAPI = client.target(BASE_URI).path("messages");
        Response response = messageAPI.request(MediaType.TEXT_PLAIN).headers(headerParams).buildGet().invoke();

//        if(!(response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL)){
//            throw new Exception(response.getStatusInfo().getReasonPhrase());
//        }
        String[] messages = new Gson().fromJson(response.readEntity(String.class), String[].class);
        return JsonToMsgFactory.buildMessageMap(messages);

    }

    /**
     * Returns all new messages in the user's inbox
     * @param requester the logged in user
     * @return all messages from all of the users being talked to
     * @throws Exception
     */
    public HashMap<String, ArrayList<Message>> getUnreadMessages(String requester) throws Exception {

        //auth header params
        MultivaluedHashMap<String, Object> headerParams = new MultivaluedHashMap<>();
        headerParams.add("Authorization", basicAuthToken);
        headerParams.add("requester", requester);

        //point the client to the profile api
        WebTarget messageAPI = client.target(BASE_URI).path("messages").path("new");
        Response response = messageAPI.request(MediaType.TEXT_PLAIN).headers(headerParams).buildGet().invoke();

        if(!(response.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL)){
            throw new Exception(response.getStatusInfo().getReasonPhrase());
        }

        String[] messages = new Gson().fromJson(response.readEntity(String.class), String[].class);
        return JsonToMsgFactory.buildMessageMap(messages);
    }


}
