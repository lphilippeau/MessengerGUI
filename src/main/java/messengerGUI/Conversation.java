package messengerGUI;

import messengerGUI.client.model.Message;
import messengerGUI.client.model.SimpleMessageFactory;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * The represents a conversation
 * Created by Luigi Philippeau on 12/5/2016.
 */
public class Conversation {

    public String partner; //the person that you are communicating with
    public ArrayList<Message> conversation;

    public Conversation(String partner){
        this.partner = partner;
        conversation = new ArrayList<>();
    }

    /**
     * Adds new messages to the conversation
     * @param newMessages
     */
    public void updateConversation(ArrayList<Message> newMessages){
        for (Message message : newMessages){
            conversation.add(message);
        }
    }

    /**
     * Adds what you just said to the conversation.
     * And sends the message to the user
     * @param sender
     * @param payload
     * @param isTemp
     * @return 
     */
    public String sendMessage(String sender, String payload, boolean isTemp){
        String type = isTemp ? "readOnce" : "regular";
        Message message;
        try {
            //create our new message
            message = SimpleMessageFactory.createMessage(sender, partner, payload, type);
            //add it to our current conversation
            conversation.add(message);
        } catch (SQLException e){
        }
        return type;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }
}
