package messengerGUI.UI;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import messengerGUI.client.MsgClient;
import messengerGUI.client.model.Profile;

/**
 * This is the main page of the GUI
 * Created by Luigi Philippeau on 12/4/2016.
 */
public class FrontPage extends Application {

    Stage window;
    static MsgClient client;
    static Scene frontPageScene;

    public static void main(String[] args){
        launch(args);
    }

	@Override
    public void start(Stage primaryStage) throws Exception {

        //instantiate the client
        client = new MsgClient();

        window = primaryStage;
        window.setTitle("Messenger");

        GridPane grin = new GridPane();
        grin.setPadding(new Insets(20,20,20,20));//sets the spacing between the window and the outer edge
        grin.setVgap(8);
        grin.setHgap(10);

        //Name label
        Label messageLabel = new Label("Log in");
        GridPane.setConstraints(messageLabel, 0, 0);

        //name input
        TextField nameInput = new TextField();
        nameInput.setPromptText("Enter username");
        GridPane.setConstraints(nameInput, 0, 1);

        //password input
        PasswordField passInput = new PasswordField();
        passInput.setPromptText("Enter password");
        GridPane.setConstraints(passInput, 0,2);

        //login
        Button login = new Button("log in");
        GridPane.setConstraints(login, 0,4);
        login.setOnAction(event -> logInAction(nameInput, passInput));

        Label registerlabel = new Label("Don't have an account?");
        GridPane.setConstraints(registerlabel, 0, 7);

        Button registerButton = new Button("Register");
        GridPane.setConstraints(registerButton, 0, 8);
        registerButton.setOnAction(event -> registerAction());

        grin.getChildren().addAll(messageLabel, nameInput, passInput, login, registerlabel, registerButton);

        frontPageScene = new Scene(grin, 300, 300);
        window.setScene(frontPageScene);

        window.show();
    }

    /**
     * The actions taken when the user tries to log in
     */
    public void logInAction(TextField username, TextField password) {

        try{
            Profile profile = client.login(username.getText(), password.getText());
            //todo take us to the main page
            ChatWindow.chatWindow(window);
        } catch (Exception e){
            Popup.display("Unable to log in", e.getMessage(), false);
        }
    }

    /**
     * What to do when the user clicks register
     */
    public void registerAction(){
        window.setScene(RegistrationPage.getRegistrationPage(window));
    }
}
