package messengerGUI.UI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * Created by Luigi Philippeau on 12/4/2016.
 */
public class Popup {
    public static boolean isUser;
    public static boolean display(String title, String message, boolean userInput) {

        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);//this means that we are blocking user interaction with other windows until this is taken care of
        window.setTitle(title);
        window.setMinWidth(400);

        Label label = new Label();
        label.setText(message);
        Button closeButton = new Button("Ok");


        VBox layout = new VBox(30);
        layout.setPadding(new Insets(20,20,20,20));
        layout.getChildren().addAll(label, closeButton);
        layout.setAlignment(Pos.CENTER);

        //If we need user input from this popup
        if(userInput){
            TextField username = new TextField();
            layout.getChildren().add(username);

            closeButton.setOnAction(e -> {
                isUserCheck(username);
                window.close();
            });
        } else {
            closeButton.setOnAction(e -> window.close());
        }


        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();

        return isUser;
    }

    private static void isUserCheck(TextField username){
        try{
            String partner = username.getText();
            isUser = FrontPage.client.isUser(partner);
            if(!isUser)throw new Exception("This user does not exist");
            //set the person in the active conversation to this user
            ChatWindow.currentReceiver = partner;
        } catch (Exception e){
            Popup.display("Unsuccessful", e.getMessage(), false);
        }
    }
}
