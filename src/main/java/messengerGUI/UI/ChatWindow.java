package messengerGUI.UI;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import messengerGUI.Conversation;
import messengerGUI.client.model.Message;
import messengerGUI.client.model.ReadOnceMessage;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * The main chat window
 * Created by Luigi Philippeau on 12/5/2016.
 */
public class ChatWindow {

    static Stage origStage;//link to the main stage

    public static String currentReceiver;
    public static String currentUser;
    public static ArrayList<Message> currentConvoList;
    public static Conversation thisConvo;
    public static HashMap<String, Conversation> allConversations;
    public static VBox sideBox;
    public static ArrayList<Button> convoButtons;
    public static TextArea talkArea;
    public static TextArea inputArea;
    public static CheckBox isTempBox;

    //save the state of user input
    private static String savedText;
    private static boolean isChecked = false;


    public ChatWindow(Stage fromStage) {

    }

    public static void chatWindow(Stage fromStage) {
        origStage = fromStage;

        origStage.setTitle("Logged in as " + FrontPage.client.sessionProfile.getName());
        currentUser = FrontPage.client.sessionProfile.getUsername();
        if (allConversations == null) allConversations = new HashMap<>();


        //GridPane layout
        GridPane pane = new GridPane();
        pane.setPadding(new Insets(20, 20, 20, 20));
        pane.setHgap(15);
        pane.setVgap(15);

        //title label
        Label messageLabel = new Label("Conversation with: " + (currentReceiver == null ? "Nobody, start one here ->" : currentReceiver));
        messageLabel.setAlignment(Pos.CENTER);
        GridPane.setConstraints(messageLabel, 0, 0);

        //Start a new conversation button
        Button newConvo = new Button("New Conversation");
        newConvo.setOnAction(event -> newConverstationAction());
        GridPane.setConstraints(newConvo, 1, 0);


        //the properties for the text area
        talkArea = new TextArea();
        talkArea.setMinWidth(300);
        talkArea.setPrefColumnCount(100);
        talkArea.setPrefRowCount(20);
        talkArea.setPrefWidth(150);
        talkArea.setEditable(false);
        updateTalkArea();
        //todo add method to update talk area
        GridPane.setConstraints(talkArea, 0, 1);


        //Textfield
        inputArea = new TextArea();
        inputArea.setPrefColumnCount(100);
        inputArea.setPrefColumnCount(1);
        inputArea.setPrefWidth(150);
        inputArea.setWrapText(true);
        inputArea.setText(savedText == null ? "" : savedText);
        GridPane.setConstraints(inputArea, 0, 3);


        //scrollpane
        ScrollPane sp = new ScrollPane();
        sp.fitToHeightProperty();

        //Add a new ho
        sideBox = new VBox();
        sideBox.setVgrow(sp, Priority.ALWAYS);
        sideBox.setAlignment(Pos.BASELINE_LEFT);
        if (convoButtons != null) sideBox.getChildren().addAll(convoButtons);
        GridPane.setConstraints(sideBox, 1, 1);


        //Checkbox for temp message
        isTempBox = new CheckBox("Temp?");
        isTempBox.setSelected(isChecked);
        GridPane.setConstraints(isTempBox, 1, 3);

        //send message button
        Button send = new Button("Send");
        send.setAlignment(Pos.CENTER);
        send.setOnAction(event -> sendMessage(inputArea, isTempBox));
        GridPane.setConstraints(send, 2, 3);


        //Add all the children to the layout
        pane.getChildren().addAll(messageLabel, newConvo, talkArea, inputArea, sideBox, isTempBox, send);

        //create the scene that tis will all be in
        Scene chatScene = new Scene(pane, 550, 450);

        //set the scene
        origStage.setScene(chatScene);
        origStage.show();

    }

    /**
     * Updates all components of the UI
     */
    public static void updateChatWindow() {

        HashMap<String, ArrayList<Message>> newMessages;
        //todo what if no messages are returned
        //Update all active conversations
        try {
            saveState(inputArea, isTempBox);
            newMessages = FrontPage.client.getUnreadMessages(currentUser);

            if (newMessages != null && newMessages.keySet().size() != 0) {

                for (String key : newMessages.keySet()) {
                    //check if we are already in a conversation with this user
                    if (!(allConversations.keySet().contains(key))) {
                        allConversations.put(key, new Conversation(key));//create a new conversation
                        addConvoButton(key);
                    }
                    //add the new messages to the conversation
                    allConversations.get(key).updateConversation(newMessages.get(key));
                }

            }
            //sort the current conversation by date and display it
            //updateTalkArea();

        } catch (Exception e) {
            //do nothing
        }

        ChatWindow.chatWindow(origStage);

    }

    /**
     * Pops up a window asking the user to pass in who they like to speak to.
     */
    public static void newConverstationAction() {
        boolean newConveration = Popup.display("New Conversation", "Please enter a username", true);

        if (newConveration) {//if the conversation was created successfully
            allConversations.put(currentReceiver, new Conversation(currentReceiver));//add this to all our conversations
            thisConvo = allConversations.get(currentReceiver);
            currentConvoList = thisConvo.conversation;

            //add method to update all conversations
            addConvoButton(currentReceiver);
        }
    }

    /**
     * Adds a new button for every conversation
     *
     * @param receiver
     */
    private static void addConvoButton(String receiver) {
        Button button = new Button(receiver);
        if (convoButtons == null) convoButtons = new ArrayList<>();
        //check that this button does not exist
        for (int i = 0; i < convoButtons.size(); i++){
            if(convoButtons.get(i).getText().equals(receiver)) return; //this button is already created.
        }
        button.setOnAction(event -> {
            currentReceiver = receiver;
            currentConvoList = allConversations.get(receiver).conversation;
            updateChatWindow();

        });

        convoButtons.add(button);
        updateChatWindow();
    }


    /**
     * Updates the text in the chat window
     */
    public static void updateTalkArea() {

        if (currentConvoList != null) {
            Comparator<Message> byDate = (Message m1, Message m2) -> m1.getDateSent().compareTo(m2.getDateSent());
            if (currentConvoList != null) currentConvoList = allConversations.get(currentReceiver).conversation;
            currentConvoList.sort(Collections.reverseOrder(byDate));

            //update our text area
            talkArea.clear();
            for (Message message : currentConvoList) {
                //if the message is a readOnce message that cannot be read
                if (message instanceof ReadOnceMessage) {
                    if(((ReadOnceMessage) message).isCanRead() == true){
                        talkArea.appendText(message.toString() + "\n");
                        ((ReadOnceMessage) message).setCanRead(false);
                    }
                } else talkArea.appendText(message.toString() + "\n");
            }
        }
    }

    /**
     * Saves all of the current user input so that it survives ui refresh
     *
     * @param inputArea
     * @param box
     */
    private static void saveState(TextArea inputArea, CheckBox box) {
        if (inputArea != null && box != null) {
            savedText = inputArea.getText();
            isChecked = box.isSelected();
        }

    }

    /**
     * Sends a message to the user
     * @param inputArea
     * @param box
     */
    public static void sendMessage(TextArea inputArea, CheckBox box) {

        String type = thisConvo.sendMessage(currentUser, inputArea.getText(), box.isSelected());
        try {
            FrontPage.client.sendMessage(currentUser, currentReceiver, inputArea.getText(), type);
        } catch (Exception e) {
            Popup.display("Error", e.getMessage(), false);
        }
        inputArea.clear();
        updateTalkArea();
    }

}
