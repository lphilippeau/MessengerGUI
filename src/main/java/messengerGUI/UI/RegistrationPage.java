package messengerGUI.UI;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * This class is where the user can create an account in the application
 * Created by Luigi Philippeau on 12/4/2016.
 */
public class RegistrationPage {

    static Stage mainStage;//the stage hosting this scene

    public static Scene getRegistrationPage(Stage fromStage){

        //todo, make the user have to put in their password twice. Error if they dont match

        mainStage = fromStage;
        Scene scene;

        GridPane grin = new GridPane();
        grin.setPadding(new Insets(20,20,20,20));//sets the spacing between the window and the outer edge
        grin.setVgap(8);
        grin.setHgap(10);

        Label messageLabel = new Label("Create an account");
        GridPane.setConstraints(messageLabel, 0, 0);

        //name input
        TextField nameInput = new TextField();
        nameInput.setPromptText("Enter your name");
        GridPane.setConstraints(nameInput, 0, 1);

        //name input
        TextField usernameInput = new TextField();
        nameInput.setPromptText("Enter your username");
        GridPane.setConstraints(usernameInput, 0, 2);

        //password input
        PasswordField passInput = new PasswordField();
        passInput.setPromptText("Enter password");
        GridPane.setConstraints(passInput, 0,3);

        //login
        Button registerButton = new Button("Create Account");
        GridPane.setConstraints(registerButton, 0,5);
        registerButton.setOnAction(event -> onCreateAccount(nameInput, usernameInput, passInput));


        grin.getChildren().addAll(messageLabel, nameInput, usernameInput, passInput,  registerButton);

        return scene = new Scene(grin, 300, 300);
    }

    public static void onCreateAccount(TextField nameInput, TextField usernameInput, TextField passwordInput){
        try{
            FrontPage.client.register(nameInput.getText(), usernameInput.getText(), passwordInput.getText());
            mainStage.setScene(FrontPage.frontPageScene);
            Popup.display("Account Created", "Log in to begin", false);
        } catch (Exception e){
            Popup.display("Unable to register", e.getMessage(), false);
        }
    }
}
