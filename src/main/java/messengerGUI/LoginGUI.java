package messengerGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import messengerGUI.client.MsgClient;

public class LoginGUI extends JFrame // create class NewUser
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField userText;
	private JTextField passwordText;
	protected java.lang.String Spassword;

	MsgClient client = new MsgClient();
	
	
	private JTextArea conversation;
	private JTextField message;
	private JButton send;
	 

	// database URL
	static final String DB_URL = "jdbc:mysql://localhost/demo";

	//   Database credentials
	static final String USER = "root";
	static final String PASS = "root";
	protected static final String String = null;

	/**
	 * Launch the application.
     * @param args
	 */
	public static void main(String[] args) // main method
	{
		EventQueue.invokeLater(new Runnable() {
                        @Override
			public void run() // define run method
			{

				new LoginGUI();

			
			}
		});
	}
	
	public void open() {
		  conversation = new JTextArea(10, 30);
		  conversation.setEnabled(false);
		  conversation.setLineWrap(true);
		  JScrollPane sp = new JScrollPane(conversation);
		  JPanel top = new JPanel();
		  top.add(new JLabel("Conversation"));
		  top.add(sp);
		 
		  message = new JTextField(15);
		  message.setText("");
		  send = new JButton("SEND");
		  JPanel middle = new JPanel();
		  middle.add(new JLabel("Message: "));
		  middle.add(message);
		  middle.add(send);
		 
		  JPanel content = new JPanel(new BorderLayout());
		  content.add(top, BorderLayout.NORTH);
		  content.add(middle, BorderLayout.CENTER);
		 
		  JFrame frame = new JFrame("Chat GUI");
		  frame.setContentPane(content);
		 
		  //event handler - button send
		 
		  //event handler - window frame
		 
		  //Display the window.
		  message.grabFocus();
		  frame.setSize(400, 250);
		  frame.setVisible(true);
		  
		  send.addActionListener(new ActionListener() {
			  
			  @Override
			  public void actionPerformed(ActionEvent e) {
			    conversation.append("me: " + message.getText() + "\n");
			    //("MESSAGE", message.getText());
			    message.setText("");
			    message.grabFocus();
			  }
			 
			});
		  
		  
		}

	/**
	 * Create the frame.
	 */
	public LoginGUI() 
	{
		
				
		JFrame frame = new JFrame("Welcome to the Messenger Application");
		frame.setSize(300, 150);		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		frame.setLayout(null);
		
		JLabel userLabel = new JLabel("User");
		userLabel.setBounds(10, 10, 80, 25);
		frame.add(userLabel);
		userText = new JTextField(20);
		userText.setBounds(100, 10, 160, 25);
		frame.add(userText);
		
		JLabel passwordLabel = new JLabel("Password");
		passwordLabel.setBounds(10, 40, 80, 25);
		frame.add(passwordLabel);
		passwordText = new JPasswordField(20);
		passwordText.setBounds(100, 40, 160, 25);
		frame.add(passwordText);
		
		JButton loginButton = new JButton("login");
		loginButton.setBounds(10, 80, 80, 25);
		frame.add(loginButton);
		JButton registerButton = new JButton("register");
		registerButton.setBounds(180, 80, 80, 25);
		frame.add(registerButton);
		
		
		registerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// declare variables
				String username = "";
				String password = "";

				// get values using getText() method
				username = userText.getText().trim();
				password = passwordText.getText().trim();

				// check condition it field equals to blank throw error message
				if (username.equals("") || password.equals("")) {
					JOptionPane.showMessageDialog(null, " name or password or Role is wrong", "Error",
							JOptionPane.ERROR_MESSAGE);
				} else {

					try{
						// pass this to the methods in here.
						client.register(null, username, password);

					}catch(Exception e1){
						JOptionPane.showMessageDialog(null, e1.getMessage()+" name or password or Role is wrong", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
					
				}
			}

		});
		
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				
				
				
				// declare variables
				String username = "";
				String password = "";

				// get values using getText() method
				username = userText.getText().trim();
				password = passwordText.getText().trim();

				// check condition it field equals to blank throw error message
				if (username.equals("") || password.equals("")) {
					JOptionPane.showMessageDialog(null, " name or password or Role is wrong", "Error",
							JOptionPane.ERROR_MESSAGE);
				} else {

					try{
						// pass this to the methods in here.
						client.login(username, password);
					}catch(Exception e1){
						JOptionPane.showMessageDialog(null, e1.getMessage()+" name or password or Role is wrong", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
					
					open();
				}
				
				

				
				
				
			}

		});
		
	
	}
	
}